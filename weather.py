# ==============================================================
'''
SCRIPT:
	weather.py
	
PURPOSE:
    - Sense check weather data; Shen notes:
    According to our agreement with the vendor, the historical weather data should start from 1 Jan 2012 up to today, covering 986 Australian postcodes that we provided them with. Please see attached the list of the 986 postcodes that we provided.

    The checks we'd like to perform include:
    Are all 986 postcodes covered in the data?
    Did the data start on 1 Jan 2012 for each postcode?
    Did the data continue up until today (day of extraction)? Any missing period of time for each postcode?
    Number of rows is equal for each postcode?
    Hourly frequency for each postcode?
    Any unreasonable lat/lon? (e.g. lat being positive is impossible within Australia)
    Any unreasonable feature values? (e.g. temperature > 60)
    Any missing features for certain postcodes?
    Is data type consistent for each data feature?
    Any other checks that you did last time.
    
INPUTS:
    http://history.openweathermap.org/storage/8b62ce035425e234ad0786b1d87b0820.tar.gz
    
OUTPUTS:
	
	
DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 04Feb2020
    ---
'''
# ==============================================================
# -------------------------------------------------

import os, sys, importlib
import pandas as pd
import numpy as np
import subprocess
import urllib.request
import tarfile

sys.path.insert(0, '/home/jovyan/a01_repos/joe_utils')
import UDF_Utils as utl

importlib.reload(sys.modules['UDF_Utils'])


# ==============================================================
# Load data
# ==============================================================
# url_tar = 'http://history.openweathermap.org/storage/8b62ce035425e234ad0786b1d87b0820.tar.gz'
# stream = urllib.request.urlopen(url_tar)
# file_tar = tarfile.open(fileobj=stream, mode="r|gz")
file_tar = '8b62ce035425e234ad0786b1d87b0820.tar.gz'
# file_tar = 'grades.tar.gz'
path_prj = '/home/jovyan/a02_projects/a04_weather'
path_tar = f'{path_prj}/a01_data/{file_tar}'

tStart = utl.Timer()
rawData = utl.Read_Tarfile_Csv(path_tarfile=path_tar)
utl.Timer(tStart)
print(rawData.shape)
rawData.head(2)
rawData.dtypes


# ==============================================================
# Summary stats
# ==============================================================
# -------------------------------------------------
# Are all 986 postcodes covered in the data?
# -------------------------------------------------
len(rawData['post_code'].unique()) # 986
rawData['post_code'].min() # 800
rawData['post_code'].max() # 7330
rawData['post_code'].dtypes
rawData.loc[rawData['post_code'] == 800,:].head(10)

# -------------------------------------------------
# Did the data start on 1 Jan 2012 for each postcode?
# -------------------------------------------------

