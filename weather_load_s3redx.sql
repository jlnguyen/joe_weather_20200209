--==============================================================
/*
SCRIPT:
    weather_load_s3redx.sql

PURPOSE:
    - Load parquet file into redshift
    - Pivot out weather_id (up to 4) per {dt, post_code}

INPUTS:
    - s3://data-preprod-redshift-exports/Joe/a02_weather/weather_20200207.parquet

OUTPUTS:

DATABASE:
    RedX: datacap

DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 04Feb2020
    ---
    Version 1.1 - Joseph Nguyen | 12Feb2020
        - Changed final table name to [weather_hour_hist]
        - Created timezone offset dictionary (manually based on internet search)
        -> https://www.australia.gov.au/about-australia/facts-and-figures/time-zones-and-daylight-saving
        - Created {time_stamp} for local time = dt_iso + timezone
    ---
*/
--==============================================================
-------------------------------------------------


create table loyalty_modeling.jn_temp as (select * from loyalty_modeling.weather_hour_2012_2020);

DROP TABLE IF EXISTS loyalty_modeling.weather_hour_2012_2020;
CREATE TABLE loyalty_modeling.weather_hour_2012_2020
(
    id                      varchar(8)
	,dt                     varchar(15)
	,dt_iso                 varchar(30)
	,timezone               varchar(10)
	,postcode               varchar(4)
    ,state                  varchar(3)
    ,lttd                   float
    ,lngtd                  float
    ,temperature            float
    ,feels_like             float
    ,temp_min               float
    ,temp_max               float
    ,pressure               float
    ,sea_level              float
    ,grnd_level             float
    ,humidity               float
    ,wind_speed             float
    ,wind_degree            float
    ,rain_1h                float
    ,rain_3h                float
    ,rain_6h                float
    ,rain_12h               float
    ,rain_24h               float
    ,rain_today             float
    ,snow_1h                float
    ,snow_3h                float
    ,snow_6h                float
    ,snow_12h               float
    ,snow_24h               float
    ,snow_today             float
    ,clouds                 smallint
    ,weather_id             varchar(3)
    ,weather_main           varchar(15)
    ,weather_desc           varchar(30)
    ,weather_icon           varchar(3)
)
DISTKEY (dt_iso)
SORTKEY (postcode, dt_iso)
;
GRANT SELECT on loyalty_modeling.weather_hour_2012_2020 to PUBLIC;

-- COPY loyalty_modeling.weather_hour_2012_2020
-- FROM 's3://data-preprod-redshift-exports/Joe/a02_weather/weather_20200207.parquet' 
-- IAM_ROLE 'arn:aws:iam::846880483810:user/jnguyen7@woolworths.com.au'
-- -- IAM_ROLE 'arn:aws:iam::0123456789012:role/MyRedshiftRole'
-- -- arn:aws:redshift:ap-southeast-2:846880483810:dbuser:data-cap-prod-cluster/jnguyen7
-- FORMAT AS PARQUET;

delete from loyalty_modeling.weather_hour_2012_2020;
COPY loyalty_modeling.weather_hour_2012_2020
FROM 's3://data-preprod-redshift-exports/Joe/a02_weather/weather_20200207_0.csv' 
CREDENTIALS 'aws_access_key_id=AKIA4KLQCVHRE53KRCQK;aws_secret_access_key=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'
CSV IGNOREHEADER 1;
COPY loyalty_modeling.weather_hour_2012_2020
FROM 's3://data-preprod-redshift-exports/Joe/a02_weather/weather_20200207_1.csv' 
CREDENTIALS 'aws_access_key_id=AKIA4KLQCVHRE53KRCQK;aws_secret_access_key=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'
CSV IGNOREHEADER 1;
COPY loyalty_modeling.weather_hour_2012_2020
FROM 's3://data-preprod-redshift-exports/Joe/a02_weather/weather_20200207_2.csv' 
CREDENTIALS 'aws_access_key_id=AKIA4KLQCVHRE53KRCQK;aws_secret_access_key=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'
CSV IGNOREHEADER 1;


-------------------------------------------------
-- View data
-------------------------------------------------
select count(*), count(distinct id), count(distinct postcode) from loyalty_modeling.weather_hour_2012_2020;
select top 188 * from loyalty_modeling.weather_hour_2012_2020;

select 
    ordinal_position as position,
    column_name,
    data_type,
    case when character_maximum_length is not null
        then character_maximum_length
        else numeric_precision end as max_length,
    is_nullable,
    column_default as default_value
from information_schema.columns
where table_schema = 'loyalty_modeling'
    -- and table_name = 'weather_hour_2012_2020'
    and table_name = 'weather_hour'
order by 1
;


--==============================================================
/*
    timezone dictionary
    https://www.australia.gov.au/about-australia/facts-and-figures/time-zones-and-daylight-saving
*/
--============================================================== 
drop table if exists loyalty_modeling.jn_ref_timezone;
create table loyalty_modeling.jn_ref_timezone
(
    timezone_code   varchar(4)
    ,offset_sec     int
    ,timezone_desc  varchar(60)
)
sortkey (offset_sec);
grant select on loyalty_modeling.jn_ref_timezone to public;

insert into loyalty_modeling.jn_ref_timezone
values ('AEST', 36000, 'Australian Eastern Standard Time') 
    ,('ACST', 34200, 'Australian Central Standard Time')
    ,('AWST', 28800, 'Australian Western Standard Time')
    ,('AEDT', 39600, 'Australian Eastern Daylight Time')
    ,('ACDT', 37800, 'Australian Central Daylight Time')
;


--==============================================================
/*
    Pivot out weather_id per dt, post_code
*/
--==============================================================   drop table if exists loyalty_modeling.weather_hour_hist;
create table loyalty_modeling.weather_hour_hist
DISTKEY (time_stamp)
SORTKEY (postcode, time_stamp)
as
(
    select
        dt
        ,dateadd(seconds, offset_sec
            ,cast(split_part(dt_iso, ' +', 1) as timestamp)
        ) as time_stamp
        ,timezone_code
        ,postcode
        ,state
        ,lttd
        ,lngtd
        ,temperature - 273.15 as temperature
        ,feels_like - 273.15 as feels_like
        ,temp_min - 273.15 as temp_min
        ,temp_max - 273.15 as temp_max
        ,pressure
        ,sea_level
        ,grnd_level
        ,humidity
        ,wind_speed
        ,wind_degree
        ,rain_1h
        ,rain_3h
        ,rain_6h
        ,rain_12h
        ,rain_24h
        ,rain_today
        ,snow_1h
        ,snow_3h
        ,snow_6h
        ,snow_12h
        ,snow_24h
        ,snow_today
        ,clouds

        /* Pivot out up to 4 columns */
        ,max(case row_id when 1 then weather_id end) as weather_id_1
        ,max(case row_id when 1 then weather_main end) as weather_main_1
        ,max(case row_id when 1 then weather_desc end) as weather_desc_1
        ,max(case row_id when 1 then weather_icon end) as weather_icon_1
        
        ,max(case row_id when 2 then weather_id end) as weather_id_2
        ,max(case row_id when 2 then weather_main end) as weather_main_2
        ,max(case row_id when 2 then weather_desc end) as weather_desc_2
        ,max(case row_id when 2 then weather_icon end) as weather_icon_2
        
        ,max(case row_id when 3 then weather_id end) as weather_id_3
        ,max(case row_id when 3 then weather_main end) as weather_main_3
        ,max(case row_id when 3 then weather_desc end) as weather_desc_3
        ,max(case row_id when 3 then weather_icon end) as weather_icon_3
        
        ,max(case row_id when 4 then weather_id end) as weather_id_4
        ,max(case row_id when 4 then weather_main end) as weather_main_4
        ,max(case row_id when 4 then weather_desc end) as weather_desc_4
        ,max(case row_id when 4 then weather_icon end) as weather_icon_4
    from
    (
        select
            a.*
            ,row_number() over (
                partition by dt, postcode
                order by weather_id
            ) as row_id
            ,rf.offset_sec
            ,rf.timezone_code
        from loyalty_modeling.weather_hour_2012_2020 as a
        left join
            loyalty_modeling.jn_ref_timezone as rf
        on rf.offset_sec = cast(a.timezone as int)
        -- limit 1000
    )z
    group by 1,2,3,4,5,6,7,8,9,10
        ,11,12,13,14,15,16,17,18,19,20
        ,21,22,23,24,25,26,27,28,29,30
);
grant select on loyalty_modeling.weather_hour_hist to public;

select count(*), count(distinct postcode) from loyalty_modeling.weather_hour_hist;
--^ [69,880,637	986] | [69,761,472	986]
select count(*), count(distinct post_code) from loyalty_modeling.weather_hour_2012_2020;
--^ [69,880,637	986]
select dt, postcode from loyalty_modeling.weather_hour_hist group by 1,2 having count(*) > 1;   -- [EMPTY]
select top 188 * from loyalty_modeling.weather_hour;

-- Observations by postcode
select post_code
    ,count(*) as cnt
    ,count(distinct dt) as dt_cnt
from loyalty_modeling.weather_hour
group by 1
order by 1;

-- Min and max temperature
select
    min(feels_like), max(feels_like)
    ,min(temp_min), max(temp_max)
    ,min(time_stamp), max(time_stamp)
from loyalty_modeling.weather_hour_hist;


select
    state
    ,timezone_code
    ,count(*)
from loyalty_modeling.weather_hour_hist
group by 1,2
order by 1,2;


select top 1000 *
from loyalty_modeling.weather_hour_hist
where postcode = '2000'
order by dt;

select top 1000 *
from loyalty_modeling.weather_hour_2012_2020
where postcode = '2000'
    and dt in (
        '1325376000'
        ,'1325379600'
        ,'1325383200'
        ,'1325386800'
        ,'1325390400'
        ,'1325394000'
        ,'1325397600'
        ,'1325401200'
        ,'1325404800'
        ,'1325408400'
        ,'1325412000'
        ,'1325415600'
        ,'1325419200'
        ,'1325422800'
        ,'1325426400'
        ,'1325430000'
    )
order by dt;

select top 1000 *
from loyalty_modeling.weather_hour
where postcode = '2000'
order by dt;
